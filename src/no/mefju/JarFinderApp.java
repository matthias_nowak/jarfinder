package no.mefju;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.prefs.Preferences;

import no.mefju.ui.JarFinderWindow;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;

public class JarFinderApp {
	final static String dataFn = "jarFolder";
	private final static ExecutorService threadPool=  Executors.newWorkStealingPool();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("JarFinder started");
		// TODO Auto-generated method stub
		Display display = Display.getDefault();
		JarFinderWindow jfw = new JarFinderWindow(display);
		jfw.open();
		jfw.layout();
		//
		jfw.mntmAddFolder.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Preferences prefs = Preferences.userNodeForPackage(JarFinderApp.class);
				DirectoryDialog fd = new DirectoryDialog(jfw);
				fd.setFilterPath(prefs.get(dataFn, null));
				String fn = fd.open();
				if (!fn.isEmpty()) {
					prefs.put(dataFn, fn);
					System.out.printf("got folder %s\n",fn);
					jfw.addFolder(fn);
				}
			}
		});
				
		Preferences prefs = Preferences.userNodeForPackage(JarFinderApp.class);
		jfw.addFolder(prefs.get(dataFn, null));
		//
		while (display.getShells().length > 0) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		System.out.println("JarFinder stopped");
	}

	/**
	 * @return the threadpool
	 */
	public static ExecutorService getThreadpool() {
		return threadPool;
	}

}
