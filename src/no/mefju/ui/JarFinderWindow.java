/**
 * 
 */
package no.mefju.ui;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import no.mefju.JarFinderApp;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import swing2swt.layout.BorderLayout;
import swing2swt.layout.BoxLayout;

/**
 * @author mnow
 *
 */
public class JarFinderWindow extends Shell {

	public MenuItem mntmAddFolder;
	private volatile boolean keepRunning;
	private int updateInterval = 500;
	private long jarFileCount;
	private long jarClassCount;
	private long jarJavaCount;
	private Entries jarEntries;
	private String jarFileName;
	private Tree tree;
	private List list;
	private SashForm sashForm;

	public JarFinderWindow(Display display) {
		// TODO Auto-generated constructor stub
		super(display, SWT.SHELL_TRIM);
		createContents();

	}

	private void createContents() {
		// TODO Auto-generated method stub
		JarFinderWindow shell = this;
		setSize(569, 462);
		setLayout(new FillLayout());

		Menu menu = new Menu(this, SWT.BAR);
		setMenuBar(menu);

		MenuItem mntmFile_1 = new MenuItem(menu, SWT.CASCADE);
		mntmFile_1.setText("File");

		Menu menu_1 = new Menu(mntmFile_1);
		mntmFile_1.setMenu(menu_1);

		mntmAddFolder = new MenuItem(menu_1, SWT.NONE);
		mntmAddFolder.setText("Add folder");

		MenuItem mntmExit = new MenuItem(menu_1, SWT.NONE);
		mntmExit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				shell.close();
			}
		});
		mntmExit.setText("E&xit");

		sashForm = new SashForm(this, SWT.VERTICAL);

		tree = new Tree(sashForm, SWT.BORDER | SWT.VIRTUAL);
		
		list = new List(sashForm, SWT.BORDER);
		sashForm.setWeights(new int[] {321, 80});

		tree.addListener(SWT.SetData, new Listener() {
			public void handleEvent(Event event) {
				TreeItem item = (TreeItem) event.item;
				TreeItem parentItem = item.getParentItem();
				int idx = 0;
				Entries entry;
				if (parentItem == null) {
					idx = tree.indexOf(item);
					entry = jarEntries;
				} else {
					idx = parentItem.indexOf(item);
					entry = (Entries) parentItem.getData();
				}
				item.setData(entry.entries[idx]);
				item.setText(entry.keys[idx]);
				item.setItemCount(entry.entries[idx].entries.length);
			}
		});

		tree.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				TreeItem item = (TreeItem) e.item;
				Entries te = (Entries) item.getData();
				list.removeAll();
				for (String p : te.places) {
					list.add(p);
				}
				Rectangle sfa = sashForm.getClientArea();
				int h = list.getItemHeight()*te.places.size()+3;
				int[] nh = new int[2];
				nh[0]=sfa.height- h - sashForm.getSashWidth()-2*sashForm.getBorderWidth();
				nh[1]=h + sashForm.getBorderWidth();
				if(nh[0]<=0)
				{
					nh[0]=nh[1]=1;
				}
				sashForm.setWeights(nh);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("widgetDefaultSelected");
			}
		});
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public void addFolder(String fn) {
		// TODO Auto-generated method stub
		Display display = Display.getCurrent();
		ProgressWindow pw = new ProgressWindow(display);
		keepRunning = true;
		jarFileCount = 0;
		jarClassCount = 0;
		jarJavaCount = 0;
		jarEntries = new Entries();
		pw.btnCancel.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				keepRunning = false;
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				keepRunning = false;
			}
		});
		pw.open();
		pw.layout();

		display.asyncExec(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (pw.isDisposed())
					return;
				pw.text.setText(String.format("%s\n  %d - %d - %d",
						jarFileName, jarFileCount, jarClassCount, jarJavaCount));
				if (!keepRunning) {
					pw.close();
				} else {
					display.timerExec(updateInterval, this);
				}
			}
		});
		JarFinderApp.getThreadpool().submit(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				bgAddFolders(fn, display);
			}
		});

	}

	protected void bgAddFolders(String fn, Display display) {
		// TODO Auto-generated method stub
		File jarDir = new File(fn);
		bgAddFiles(jarDir);
		jarEntries.prepare();
		keepRunning = false;
		display.asyncExec(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				// put result into tree
				tree.removeAll();
				tree.setItemCount(jarEntries.entries.length);
			}
		});

	}

	private void bgAddFiles(File jarFile) {
		if (!keepRunning)
			return;
		if (jarFile.isDirectory()) {
			File[] listFiles = jarFile.listFiles();
			for (File f : listFiles)
				bgAddFiles(f);
		} else {
			if (jarFile.exists()) {
				jarFileName = jarFile.getName();
				String fn = jarFileName.toLowerCase();
				if (fn.endsWith(".jar")) {
					// System.out.printf("got a jar file %s\n",jarDir.getPath());
					++jarFileCount;
					ZipFile zipFile = null;
					try {
						zipFile = new ZipFile(jarFile);
						String longName = jarFile.getPath();
						Enumeration<? extends ZipEntry> zipEntries = zipFile
								.entries();
						while (zipEntries.hasMoreElements() && keepRunning) {
							ZipEntry entry = zipEntries.nextElement();
							String entryName = entry.getName();
							String lcName = entryName.toLowerCase();
							if (lcName.endsWith(".class")) {
								++jarClassCount;
								jarEntries.find(entryName).add(longName);
							}
							if (lcName.endsWith(".java")) {
								++jarJavaCount;
								jarEntries.find(entryName).add(longName);
							}
						}

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						try {
							zipFile.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}

	}

	class Entries {
		private Map<String, Entries> tmpEntries;
		private LinkedList<String> places;
		private String[] keys;
		private Entries[] entries;

		public Entries() {
			tmpEntries = new TreeMap<String, JarFinderWindow.Entries>();
			places = new LinkedList<String>();
		}

		public void prepare() {
			// TODO Auto-generated method stub
			int sz = tmpEntries.size();
			keys = new String[sz];
			entries = new Entries[sz];
			int i = 0;
			for (Entry<String, Entries> e : tmpEntries.entrySet()) {
				keys[i] = e.getKey();
				entries[i] = e.getValue();
				entries[i].prepare();
				++i;
			}
			tmpEntries.clear();
		}

		public LinkedList<String> find(String fn) {
			Entries m = this;
			String[] split = fn.split("/");
			for (String p : split) {
				Entries e = m.tmpEntries.get(p);
				if (e == null) {
					e = new Entries();
					m.tmpEntries.put(p, e);
				}
				m = e;
			}
			return m.places;
		}
	}

	// public static void main(String[] args) {
	// Display display = new Display();
	// final Shell shell = new Shell(display);
	// shell.setLayout(new FillLayout());
	// final Tree tree = new Tree(shell, SWT.VIRTUAL | SWT.BORDER);
	// tree.addListener(SWT.SetData, new Listener() {
	// @Override
	// public void handleEvent(Event event) {
	// TreeItem item = (TreeItem) event.item;
	// TreeItem parentItem = item.getParentItem();
	// String text = null;
	// if (parentItem == null) {
	// text = "node " + tree.indexOf(item);
	// } else {
	// text = parentItem.getText() + " - "
	// + parentItem.indexOf(item);
	// }
	// item.setText(text);
	// item.setItemCount(10);
	// }
	// });
	// tree.setItemCount(20);
	// shell.setSize(400, 300);
	// shell.open();
	// while (!shell.isDisposed()) {
	// if (!display.readAndDispatch())
	// display.sleep();
	// }
	// display.dispose();
	// }

}
