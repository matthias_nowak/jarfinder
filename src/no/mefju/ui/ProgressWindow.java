/**
 * 
 */
package no.mefju.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * @author mnow
 *
 */
public class ProgressWindow extends Shell {
	public Text text;
	public Button btnCancel;
	
	public ProgressWindow(Display display) {
		// TODO Auto-generated constructor stub
		super(display, SWT.SHELL_TRIM);
		setText("Progress");
		setSize(415, 151);
		setLayout(new FillLayout(SWT.VERTICAL));
		
		text = new Text(this, SWT.BORDER | SWT.READ_ONLY | SWT.CENTER | SWT.MULTI);
		
		btnCancel = new Button(this, SWT.NONE);
		btnCancel.setText("Cancel");
	}
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
